# e3-evrsnifferioc

EPICS driver for IFC14x0 MTCA systems running the "EVR Sniffer" firmware

More info on the [FPGA project](https://gitlab.esss.lu.se/hwcore/fpga/ifc14x0_evr_sniffer)

## Requirements

This IOC must run on a MTCA host (CPU) that has the [Xilinx XDMA kernel driver](https://github.com/Xilinx/dma_ip_drivers.git) installed.

## EPICS dependencies

This module depends on [asyn](https://gitlab.esss.lu.se/e3/wrappers/core/e3-asyn.git)

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

```sh
require evrsnifferioc

drvEVRSnifferConfig("ASYN_PORT_NAME", "/path/to/xdma/device")

dbLoadRecords("$(evrsnifferioc_DB)/evrsniffer.template", "P=$(P), R=$(R):, PORT=ASYN_PORT_NAME")

```

## Additional information

<!-- Put design info or links (where the real pages could be in e.g. `docs/design.md`, `docs/usage.md`) to design info here.
-->

## Contributing

Contributions through pull/merge requests only.
