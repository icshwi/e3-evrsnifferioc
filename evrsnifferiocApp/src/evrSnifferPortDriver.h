/**
 * @file evrSnifferPortDriver.h
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief asynPortDriver class
 * @version 0.1
 * @date 2022-09-15
 * 
 * @copyright Copyright (c) ESS ERIC 2022
 * 
 */
#include <string>
#include <asynPortDriver.h>

// hardware access library
extern "C" {
    #include "libxildrv.h"
}

// drvInfo strings of the parameters
#define P_EnableStr       "EVRSNIFF.ENABLE"   // asynInt32,    r/w 
#define P_TX17CntStr      "EVRSNIFF.TX17"     // asynInt32,    r/w 
#define P_TX18CntStr      "EVRSNIFF.TX18"     // asynInt32,    r/w 
#define P_TX19CntStr      "EVRSNIFF.TX19"     // asynInt32,    r/w 
#define P_TX20CntStr      "EVRSNIFF.TX20"     // asynInt32,    r/w 
#define P_RX17CntStr      "EVRSNIFF.RX17"     // asynInt32,    r/w 
#define P_RX18CntStr      "EVRSNIFF.RX18"     // asynInt32,    r/w 
#define P_RX19CntStr      "EVRSNIFF.RX19"     // asynInt32,    r/w 
#define P_RX20CntStr      "EVRSNIFF.RX20"     // asynInt32,    r/w 
#define P_PollPeriodStr   "EVRSNIFF.POLL"     // asynFloat64,  r/w 

// IFC14x0 firmware registers definition
#define IFC14x0_CTL_REG     0x00000020
#define IFC14x0_TX17_REG    0x00000000
#define IFC14x0_TX18_REG    0x00000004
#define IFC14x0_TX19_REG    0x00000008
#define IFC14x0_TX20_REG    0x0000000c
#define IFC14x0_RX17_REG    0x00000010
#define IFC14x0_RX18_REG    0x00000014
#define IFC14x0_RX19_REG    0x00000018
#define IFC14x0_RX20_REG    0x0000001c

class evrSnifferPortDriver : public asynPortDriver {
public:
    evrSnifferPortDriver(const std::string &drvPortName, const std::string &devName);

    evrSnifferPortDriver(const evrSnifferPortDriver&) = delete;
    evrSnifferPortDriver& operator=(const evrSnifferPortDriver&) = delete;
    ~evrSnifferPortDriver();

    // These are the methods that we override from asynPortDriver
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);

    // Polling thread
    void pollTask(void);

protected:
    // asyn parameters list
    int P_Enable;
    int P_TX17Cnt;
    int P_TX18Cnt;
    int P_TX19Cnt;
    int P_TX20Cnt;
    int P_RX17Cnt;
    int P_RX18Cnt;
    int P_RX19Cnt;
    int P_RX20Cnt;
    int P_PollPeriod;

private:
    // XDMA device handler
    xildev *m_xdmaDev;

    // Initialization function - hardware access
    void initDevice(const std::string &devName);

    // asynPortDriver constructor parameters
    static const int MaxAddr = 1;
    static const int InterfaceMask = asynInt32Mask | asynFloat64Mask | asynDrvUserMask;
    static const int InterruptMask = asynInt32Mask | asynFloat64Mask;
    static const int AsynFlags = ASYN_CANBLOCK;
    static const int AutoConnect = 1;
    static const int Priority = 0;
    static const int StackSize = 0;
};
